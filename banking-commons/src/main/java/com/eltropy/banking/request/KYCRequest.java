/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.request;

import lombok.Data;

/**
 * @author(s) bkumar1
 * 
 * @since 06 July 2020
 * @version 1.0
 */

@Data
public class KYCRequest {
	
	private String customerId;
	private String permanentAddress;
	private String education;
	private String fatherName;
	private String motherName;
	private String aadhaar;
	private String pan;
}
