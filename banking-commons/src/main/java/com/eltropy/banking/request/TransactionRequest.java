/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.request;
import com.eltropy.banking.enums.TransactionType;

import lombok.Data;

/**
 * @author(s) bkumar1
 * 
 * @since 06 July 2020
 * @version 1.0
 */

@Data
public class TransactionRequest {
	
	private TransactionType transactionType;
    private Integer fromAccountNumber;
    private Integer toAccountNumber;
    private Integer transactionAmount;
    private Integer authPin;
    private Integer transactionPin;
    private String email;
}
