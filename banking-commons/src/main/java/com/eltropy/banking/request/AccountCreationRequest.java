/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.request;
import java.util.Date;

import com.eltropy.banking.enums.AccountType;

import lombok.Data;

/**
 * @author(s) bkumar1
 * 
 * @since 06 July 2020
 * @version 1.0
 */

@Data
public class AccountCreationRequest {
	
	private AccountType accountType;
	private boolean active;
	private Date expiryDate;
}
