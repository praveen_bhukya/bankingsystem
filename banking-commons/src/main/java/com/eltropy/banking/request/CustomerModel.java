/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.request;
import lombok.Data;

/**
 * @author(s) bkumar1
 * 
 * @since 06 July 2020
 * @version 1.0
 */

@Data
public class CustomerModel {
	
	private String firstName;
	private String lastName;
	private String userName;
	private String email;
    private String mobileNumber;
    private String id;
}
