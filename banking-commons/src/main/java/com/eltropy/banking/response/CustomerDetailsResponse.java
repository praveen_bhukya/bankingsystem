/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd.
 * All rights reserved.
 * 
 */

package com.eltropy.banking.response;

import java.util.Map;

import com.eltropy.banking.enums.AccountType;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author(s) bkumar1
 * 
 * @since 06 July 2020
 * @version 1.0
 */

@Data
public class CustomerDetailsResponse {

	@JsonProperty(value = "CustomerId")
	private String customerId;
	
	@JsonProperty(value = "FirstName")
	private String firstName;
	
	@JsonProperty(value = "LastName")
	private String lastName;
	
	@JsonProperty(value = "email")
	private String email;
	
	@JsonProperty(value = "phone")
	private String phone;
	
	@JsonProperty(value = "Accounts")
	private Map<Integer,AccountType> accounts;
}
