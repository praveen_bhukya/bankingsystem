/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.response;
import com.eltropy.banking.enums.TransactionStatus;
import com.eltropy.banking.enums.TransactionType;

import lombok.Data;

/**
 * @author(s) bkumar1
 * 
 * @since 06 July 2020
 * @version 1.0
 */

@Data
public class TransactionResponse {
	
    private Integer fromAccountNumber;
    private Integer toAccountNumber;
    private Integer transactionAmount;
	private TransactionType transactionType;
    private String email;
    private TransactionStatus txnStatus;
    private String bankReferenceNumber;
    private Double remainingBalance;
}
