/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd.
 * All rights reserved.
 * 
 */

package com.eltropy.banking.response;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author(s) bkumar1
 * 
 * @since 06 July 2020
 * @version 1.0
 */

@Data
public class CustomerBalanceResponse {

	@JsonProperty(value = "CustomerId")
	private String customerId;
	
	@JsonProperty(value = "AccountBalances")
	private Map<Integer,Double> accountBalances;
}
