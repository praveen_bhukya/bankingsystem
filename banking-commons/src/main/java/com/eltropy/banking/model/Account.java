package com.eltropy.banking.model;

import com.eltropy.banking.enums.AccountType;

import lombok.Data;

@Data
public abstract class Account {

	private Integer accountNumber;
    private Integer authPin;
    private Integer transactionPin;
    private Double balance;
    private AccountType accountYpe;
    
}
