/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */

package com.eltropy.banking.api;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eltropy.banking.persistence.dao.AccountDao;
import com.eltropy.banking.persistence.entities.Account;
import com.eltropy.banking.response.CustomerBalanceResponse;
import com.moneytap.api.spec.AbstractApi;
import com.mwyn.rest.exception.DisplayMessageException;

/**
 * @author(s) bkumar1
 * 
 * @version 1.0
 * @description: This API handles getting customer balance
 * @since 06 July 2020
 */

@Slf4j
@Service("customerBalanceApi")
public class CustomerBalanceApi extends AbstractApi<String, CustomerBalanceResponse> {

	@Autowired
	private AccountDao accountDao;

	@Override
	public CustomerBalanceResponse callApi(String customerId) {
		if(customerId==null) {
			throw new DisplayMessageException("Invalid Input.Customer Id is required");
		}
		List<Account> accounts = accountDao.getAllAccountsByCustomer(customerId);
		return buildBalanceResponse(customerId,accounts);
	}

	private CustomerBalanceResponse buildBalanceResponse(String customerId, List<Account> accounts) {
		CustomerBalanceResponse res=new CustomerBalanceResponse();
		res.setCustomerId(customerId);
		Map<Integer,Double> accountBalancesMap=new HashMap<>();
		for(Account acc:accounts) {
			accountBalancesMap.put(acc.getAccountNumber(), acc.getBalance());
		}
		res.setAccountBalances(accountBalancesMap);
		return res;
	}


}
