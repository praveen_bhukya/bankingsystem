/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */

package com.eltropy.banking.api;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;

import com.eltropy.banking.request.AccountCreationRequest;
import com.eltropy.banking.response.AccountCreationResponse;
import com.moneytap.api.spec.AbstractApi;

/**
 * @author(s) bkumar1
 * 
 * @version 1.0
 * @description: This API handles account creation
 * @since 06 July 2020
 */

@Slf4j
@Service("accountCreationApi")
public class AccountCreationApi extends AbstractApi<AccountCreationRequest, AccountCreationResponse> {


	@Override
	public AccountCreationResponse callApi(AccountCreationRequest req) {
		//TODO
		return null;
	}

}
