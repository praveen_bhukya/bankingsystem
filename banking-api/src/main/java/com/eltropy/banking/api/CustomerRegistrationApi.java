/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.api;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eltropy.banking.persistence.dao.CustomerDao;
import com.eltropy.banking.persistence.entities.Customer;
import com.eltropy.banking.request.CustomerModel;
import com.moneytap.api.spec.AbstractApi;
import com.mwyn.rest.exception.DisplayMessageException;
/**
 * @author(s) bkumar1
 * 
 * @version 1.0
 */

@Slf4j
@Service("customerRegistrationApi")
public class CustomerRegistrationApi extends AbstractApi<CustomerModel, CustomerModel> {

	@Autowired
	private CustomerDao customerDao;

	@Override
	public CustomerModel callApi(CustomerModel req) {
		Customer customer = customerDao.getCustomerByEmail(req.getEmail());
		if(customer!=null) {
			throw new DisplayMessageException("Invalid input. Customer has already been registered");
		}
		return registerNewCustomer(req);
	}

	private CustomerModel registerNewCustomer(CustomerModel req) {
		Customer customer=new Customer();
		customer.setEmail(req.getEmail());
		customer.setFirstName(req.getFirstName());
		customer.setLastName(req.getLastName());
		customer.setMobileNumber(req.getMobileNumber());
		customer.setUserName(req.getUserName());
		customer=customerDao.saveOrUpdate(customer);
		req.setId(customer.getId());
		return req;
	}


}
