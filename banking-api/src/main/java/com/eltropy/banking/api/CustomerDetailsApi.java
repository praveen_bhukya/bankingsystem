/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */

package com.eltropy.banking.api;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eltropy.banking.enums.AccountType;
import com.eltropy.banking.persistence.dao.AccountDao;
import com.eltropy.banking.persistence.dao.CustomerDao;
import com.eltropy.banking.persistence.entities.Account;
import com.eltropy.banking.persistence.entities.Customer;
import com.eltropy.banking.response.CustomerDetailsResponse;
import com.moneytap.api.spec.AbstractApi;
import com.mwyn.rest.exception.DisplayMessageException;


/**
 * @author(s) bkumar1
 * 
 * @version 1.0
 * @description: This API gives the details of a customer
 * @since 06 July 2020
 */

@Slf4j
@Service("customerDetailsApi")
public class CustomerDetailsApi extends AbstractApi<String, CustomerDetailsResponse> {

	@Autowired
	private CustomerDao customerDao;
	@Autowired
	private AccountDao accountDao;

	@Override
	public CustomerDetailsResponse callApi(String customerId) {
		Customer customer=customerDao.findById(customerId);
		if(customer==null) {
			throw new DisplayMessageException("No customer found for the given id:{}"+customerId);
		}
		List<Account> accounts = accountDao.getAllAccountsByCustomer(customerId);
		return buildCustomerDetailsResponse(customer,accounts);
	
	}

	private CustomerDetailsResponse buildCustomerDetailsResponse(Customer customer, List<Account> accounts) {
		CustomerDetailsResponse res=new CustomerDetailsResponse();
		res.setCustomerId(customer.getId());
		res.setEmail(customer.getEmail());
		res.setFirstName(customer.getFirstName());
		res.setLastName(customer.getLastName());
		res.setPhone(customer.getMobileNumber());
		Map<Integer, AccountType> accDetails=new HashMap<>();
		for(Account acc:accounts) {
			accDetails.put(acc.getAccountNumber(), acc.getAccounType());
		}
		res.setAccounts(accDetails);
		return res;
	}


}
