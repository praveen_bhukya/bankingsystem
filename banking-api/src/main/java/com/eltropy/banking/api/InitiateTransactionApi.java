/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */

package com.eltropy.banking.api;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;

import com.eltropy.banking.request.TransactionRequest;
import com.eltropy.banking.response.TransactionResponse;
import com.moneytap.api.spec.AbstractApi;

/**
 * @author(s) bkumar1
 * 
 * @version 1.0
 * @description: This API handles transfer
 * @since 06 July 2020
 */

@Slf4j
@Service("initiateTransactionApi")
public class InitiateTransactionApi extends AbstractApi<TransactionRequest, TransactionResponse> {

	@Override
	public TransactionResponse callApi(TransactionRequest req) {
		// TODO
		return null;

	}

}
