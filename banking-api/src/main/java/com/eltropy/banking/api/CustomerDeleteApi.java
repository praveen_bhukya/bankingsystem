/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd.
 * All rights reserved.
 */

package com.eltropy.banking.api;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eltropy.banking.persistence.dao.AccountDao;
import com.eltropy.banking.persistence.dao.CustomerDao;
import com.eltropy.banking.persistence.entities.Account;
import com.eltropy.banking.persistence.entities.Customer;
import com.moneytap.api.spec.AbstractApi;
import com.mwyn.rest.exception.DisplayMessageException;/**
 * @author(s) bkumar1
 * 
 * @version 1.0
 * @description: This API handles deletion of customer
 * @since 06  July 2020
 */

@Slf4j
@Service("customerDeleteApi")
public class CustomerDeleteApi extends AbstractApi<String, String> {

	@Autowired
	private CustomerDao customerDao;
	@Autowired
	private AccountDao accountDao;

	@Override
	public String callApi(String customerId) {
		if(customerId==null) {
			throw new DisplayMessageException("Invalid input. Customer Id is required");
		}
		Customer customer = customerDao.findById(customerId);
		if(customer!=null) {
			List<Account> customerAccounts = accountDao.getAllAccountsByCustomer(customerId);
			deactiveCustomerAccounts(customerAccounts);
			customer.setActive(false);
			return "SUCCESSFUL";
		}
		return "UN-SUCCESSFUL";
}

	private void deactiveCustomerAccounts(List<Account> customerAccounts) {
		for(Account acc:customerAccounts) {
			acc.setActive(false);
		}
		accountDao.saveOrUpdateAll(customerAccounts);
	}
}