/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.api;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eltropy.banking.persistence.dao.CustomerDao;
import com.eltropy.banking.persistence.dao.CustomerKycDao;
import com.eltropy.banking.persistence.entities.Customer;
import com.eltropy.banking.persistence.entities.CustomerKyc;
import com.eltropy.banking.request.KYCRequest;
import com.moneytap.api.spec.AbstractApi;

/**
 * @author(s) bkumar1
 * 
 * @version 1.0
 */

@Slf4j
@Service("customerKYCApi")
public class CustomerKYCApi extends AbstractApi<KYCRequest, String> {

	@Autowired
	private CustomerDao customerDao;
	@Autowired
	private CustomerKycDao customerKycDao;

	@Override
	public String callApi(KYCRequest req) {
		Customer customer = customerDao.findById(req.getCustomerId());
		if (customer != null) {
			CustomerKyc ckyc = customerKycDao.findActiveByCustomer(customer.getId());
			deactivateOlderKyc(ckyc);
			persistNewKyc(req);
			return "SUCCESSFUL";
		}
		return "UNSUCCESSFUL";
	}

	private void persistNewKyc(KYCRequest req) {
		CustomerKyc kyc=new CustomerKyc();
		kyc.setAadhaar(req.getAadhaar());
		kyc.setEducation(req.getEducation());
		kyc.setCustomerId(req.getCustomerId());
		kyc.setFatherName(req.getFatherName());
		kyc.setMotherName(req.getMotherName());
		kyc.setPan(req.getPan());
		kyc.setPermanentAddress(req.getPermanentAddress());
		customerKycDao.saveOrUpdate(kyc);
	}

	private void deactivateOlderKyc(CustomerKyc ckyc) {
		if (ckyc != null) {
		   ckyc.setActive(false);
		   customerKycDao.saveOrUpdate(ckyc);
		}
	}

}
