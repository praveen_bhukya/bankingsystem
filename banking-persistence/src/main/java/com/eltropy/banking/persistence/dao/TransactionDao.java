/**
 * This is a confidential property of MWYN Technologies Pvt.Ltd.
 * Copyright (C) 2016 MWYN Tech Pvt Ltd.
 * All rights reserved.
 * 
 */

package com.eltropy.banking.persistence.dao;

import com.eltropy.banking.persistence.entities.Transaction;

public interface TransactionDao extends GenericDao<Transaction, String> {

}
