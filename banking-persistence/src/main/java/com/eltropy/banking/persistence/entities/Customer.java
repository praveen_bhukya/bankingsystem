/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.persistence.entities;

import lombok.Data;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;


@Data
@Entity
@Table(name = "customer")
public class Customer extends BaseEntity<String> {

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "com.eltropy.banking.persistence.id.IdGenerator")
	@GeneratedValue(generator = "idGenerator")
	private String id;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "user_name")
	private String userName;
	
	@Column(name = "email")
    private String email;

	@Column(name = "mobile_number")
    private String mobileNumber;
	
	@Column(name = "active")
    private boolean active;
}
