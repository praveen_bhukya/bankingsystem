/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.persistence.entities;

import lombok.Data;

import java.util.Date;

import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;

import com.eltropy.banking.enums.AccountType;


@Data
@Entity
@Table(name = "account")
public class Account extends BaseEntity<String> {

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "com.eltropy.banking.persistence.id.IdGenerator")
	@GeneratedValue(generator = "idGenerator")
	private String id;
	
	@Column(name = "account_number")
	private Integer accountNumber;
	
	@Column(name = "auth_pin")
    private Integer authPin;
    
	@Column(name = "transaction_pin")
    private Integer transactionPin;
    
	@Column(name = "balance")
    private Double balance;
	
	@Column(name = "active")
    private boolean active;
	
	@Column(name = "expiry")
    private Date expiry;
    
	@Column(name = "account_type", nullable = false, length = 20)
	@Enumerated(EnumType.STRING)
    private AccountType accounType;
	
}
