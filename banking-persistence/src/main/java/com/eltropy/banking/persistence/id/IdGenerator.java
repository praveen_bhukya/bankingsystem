/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */

package com.eltropy.banking.persistence.id;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Service
public class IdGenerator implements IdentifierGenerator, ApplicationContextAware {

	private static TimeBasedIdGeneratorService timeBasedIdGeneratorService;

	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
		return IdGenerator.timeBasedIdGeneratorService.getUniqueId();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		IdGenerator.timeBasedIdGeneratorService = applicationContext.getBean(TimeBasedIdGeneratorService.class);
	}
}
