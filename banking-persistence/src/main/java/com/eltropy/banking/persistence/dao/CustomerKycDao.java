/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.persistence.dao;

import com.eltropy.banking.persistence.entities.CustomerKyc;

public interface CustomerKycDao extends GenericDao<CustomerKyc, String> {
	
	CustomerKyc findActiveByCustomer(String customerId);
}
