/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.persistence.dao.impl;

import org.springframework.stereotype.Repository;
import com.eltropy.banking.persistence.dao.CustomerKycDao;
import com.eltropy.banking.persistence.entities.CustomerKyc;

@Repository("accountDao")
public class CustomerKycImpl extends GenericDaoImpl<CustomerKyc, String> implements CustomerKycDao {

	@Override
	public CustomerKyc findActiveByCustomer(String customerId) {
		// TODO Auto-generated method stub
		return null;
	}

}
