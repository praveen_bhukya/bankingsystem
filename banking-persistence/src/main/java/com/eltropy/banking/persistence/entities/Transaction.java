/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */

package com.eltropy.banking.persistence.entities;

import lombok.Data;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;


@Data
@Entity
@Table(name = "customer")
public class Transaction extends BaseEntity<String> {

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "com.eltropy.banking.persistence.id.IdGenerator")
	@GeneratedValue(generator = "idGenerator")
	private String id;
}
