package com.eltropy.banking.persistence.entities;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class BaseEntity<PK extends Serializable> implements GenericEntity<PK>, Cloneable {

    @Column(name = "isdeleted", nullable = false)
    private Boolean isDeleted = false;

    @Column(name = "isarchived", nullable = false)
    private Boolean isArchived = false;

    @Column(name = "createdat", nullable = false, updatable = false)
    @CreatedDate private Date createdAt;

    @Column(name = "modifiedat", nullable = false)
    @LastModifiedDate private Date modifiedAt;

    @Version
    @Column(name="lock_id", columnDefinition="int(11) default 0", nullable=false)
    private Integer lockId = 1;

    @Override
    public Boolean isDeleted() {
        return isDeleted;
    }

    @Override
    public void setDeleted(Boolean deleted) {
        this.isDeleted = deleted;
    }

    @Override
    public Boolean isArchived() {
        return this.isArchived;
    }

    @Override
    public void setArchived(Boolean archived) {
        this.isArchived = archived;
    }

    @Override
    public Date getCreatedAt() {
        return this.createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public Date getModifiedAt() {
        return this.modifiedAt;
    }

    @Override
    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

}
