/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.persistence.entities;

import lombok.Data;

import java.util.Date;

import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;


@Data
@Entity
@Table(name = "customer_kyc")
public class CustomerKyc extends BaseEntity<String> {

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "com.eltropy.banking.persistence.id.IdGenerator")
	@GeneratedValue(generator = "idGenerator")
	private String id;
	
	@Column(name = "customer_id")
	private String customerId;
	
	@Column(name = "permanent_address")
	private String permanentAddress;
	
	@Column(name = "education")
	private String education;
	
	@Column(name = "father_name")
	private String fatherName;
	
	@Column(name = "mother_name")
	private String motherName;
	
	@Column(name = "aadhaar")
	private String aadhaar;
	
	@Column(name = "pan")
	private String pan;	
	
	@Column(name = "active")
	private boolean active;	
}
