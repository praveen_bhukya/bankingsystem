/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.persistence.dao;

import java.util.List;

import com.eltropy.banking.persistence.entities.Account;

public interface AccountDao extends GenericDao<Account, String> {
	
	public List<Account> getAllAccountsByCustomer(String customerId);

}
