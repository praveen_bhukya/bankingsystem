/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.persistence.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import com.eltropy.banking.persistence.dao.AccountDao;
import com.eltropy.banking.persistence.entities.Account;

@Repository("accountDao")
public class AccountDaoImpl extends GenericDaoImpl<Account, String> implements AccountDao {

	@Override
	public List<Account> getAllAccountsByCustomer(String customerId) {
		// TODO Auto-generated method stub
		return null;
	}

}
