/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */

package com.eltropy.banking.persistence.dao.impl;

import org.springframework.stereotype.Repository;

import com.eltropy.banking.persistence.dao.CustomerDao;
import com.eltropy.banking.persistence.entities.Customer;

@Repository("customerDao")
public class CustomerDaoImpl extends GenericDaoImpl<Customer, String> implements CustomerDao {

	@Override
	public Customer getCustomerByEmail(String emailId) {
		// TODO Auto-generated method stub
		return null;
	}

}
