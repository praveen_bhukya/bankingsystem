/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.persistence.dao;

import com.eltropy.banking.persistence.entities.Customer;

public interface CustomerDao extends GenericDao<Customer, String> {
	
	Customer getCustomerByEmail(String emailId);

}
