/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.persistence.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import com.eltropy.banking.persistence.entities.GenericEntity;

public interface GenericDao<E extends GenericEntity<PK>, PK extends Serializable> {
	E findById(PK id);

	List<E> findAll();

	E save(E entity);

	E update(E entity);

	E saveOrUpdate(E entity);

	void saveOrUpdateAll(Collection<E> entities);

	void delete(E entity);

	void deleteById(PK id);

	void hardDelete(E entity);
}
