/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.persistence.dao.impl;

import org.springframework.stereotype.Repository;

import com.eltropy.banking.persistence.dao.CustomerDao;
import com.eltropy.banking.persistence.dao.TransactionDao;
import com.eltropy.banking.persistence.entities.Customer;
import com.eltropy.banking.persistence.entities.Transaction;

@Repository("transactionDao")
public class TransactionDaoImpl extends GenericDaoImpl<Transaction, String> implements TransactionDao {

}
