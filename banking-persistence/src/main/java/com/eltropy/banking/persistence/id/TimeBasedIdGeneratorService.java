/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.persistence.id;

import com.netflix.config.DynamicPropertyFactory;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class TimeBasedIdGeneratorService {
	private static final AtomicInteger sequenceTill999 = new AtomicInteger(0);
	private static final int MAX_SEQUENCE = 999;

	public String getUniqueId() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmssSSS");
		String datePrefix = dateFormat.format(new Date());
		String serverId = getServiceId(); 
		String uniqueNess = String.format("%04d", Math.abs(Thread.currentThread().getId() % 10000));
		String suffix = getNextSequence();
		return datePrefix + serverId + uniqueNess + suffix;
	}

	private String getNextSequence() {
		if (sequenceTill999.get() == MAX_SEQUENCE) {
			sequenceTill999.set(0);
		}
		return String.format("%03d", sequenceTill999.incrementAndGet());
	}

	private String getServiceId() {
		return DynamicPropertyFactory.getInstance().getStringProperty("persistence.ThreeCharServiceId", "E01")
				.getValue();
	}
}