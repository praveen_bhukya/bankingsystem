/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */

package com.eltropy.banking.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author bkumar1
 * @since 06 July 2020
 * @version 1.0
 * @description: This is the swagger configuration class for documentation.
 *
 */

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.eltropy.banking")).paths(PathSelectors.regex("/v1.*"))
				.build().apiInfo(metaData());
	}

	private ApiInfo metaData() {
		return new ApiInfo("UPI", "Spring Boot REST API for BANKING", "1.0", "Terms of service",
				new Contact("Praveen Bhukya", "https://www.eltropy.com/", "praveen@moneytap.com"),
				"Copyright (C) Eltropy Pvt Ltd.", "All rights reserved");
	}
}
