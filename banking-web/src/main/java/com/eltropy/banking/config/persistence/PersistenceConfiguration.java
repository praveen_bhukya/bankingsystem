/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.config.persistence;

/**
 * @author bkumar1
 * @since 06 July 2020
 * @version 1.0
 * @description: This is the persistence configuration class.
 *
 */

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaAuditing
@EnableJpaRepositories(basePackages = { "com.eltropy" })
@EntityScan(basePackages = { "com.eltropy" })
@EnableTransactionManagement
public class PersistenceConfiguration {

}
