/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd.
 * All rights reserved.
 * 
 */
package com.eltropy.banking.application;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author bkumar1
 * @since 06 July 2020
 * @version 1.0
 * @description: This is the Banking application class.
 *
 */

@SpringBootApplication
@ComponentScan("com")
@EnableScheduling
@EnableAutoConfiguration(exclude={MongoAutoConfiguration.class})
public class BankingApplication {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(BankingApplication.class);
		app.setDefaultProperties(Collections.singletonMap("server.port", "9085"));
		app.run(args);
	}

}
