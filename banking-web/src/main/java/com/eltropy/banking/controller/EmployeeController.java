/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
/**
 * @author(s) bkumar1
 * 
 * @version 1.0
 * @description: This is the controller to mange employee activities
 * @since 28 Jan 2020
 */
@Api(value = "upi", description = "Callback Controller")
@RestController
@RequestMapping(path = "/v1/employees")
public class EmployeeController {


}
