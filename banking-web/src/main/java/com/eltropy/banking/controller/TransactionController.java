/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */


package com.eltropy.banking.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.eltropy.banking.api.InitiateTransactionApi;
import com.eltropy.banking.request.TransactionRequest;
import com.eltropy.banking.response.TransactionResponse;

/**
 * @author(s) bkumar1
 * 
 * @version 1.0
 * @description: Controller to handle transactions
 * @since 6 July 2020
 */
@Api(value = "transaction", description = "Transaction Controller")
@RestController
@RequestMapping(path = "/v1/transaction")
public class TransactionController {
	
	@Autowired
	private InitiateTransactionApi inititateTransactionApi;
	
	@PostMapping(path = "/", consumes = "application/json", produces = "application/json")
	@ApiOperation(value = "Transfer money", response = HashMap.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal server error") })
	public ResponseEntity<TransactionResponse> makeTransfer(@RequestBody TransactionRequest req) {
		return ResponseEntity.ok(inititateTransactionApi.callApi(req));
	}

}
