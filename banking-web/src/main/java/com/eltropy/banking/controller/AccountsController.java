/**
 * This class is a confidential property of MWYN Technologies Pvt.Ltd.
 * Copyright (C) 2016 MWYN Tech Pvt Ltd.
 * All rights reserved.
 */

package com.eltropy.banking.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.eltropy.banking.api.AccountCreationApi;
import com.eltropy.banking.request.AccountCreationRequest;
import com.eltropy.banking.request.CustomerModel;
import com.eltropy.banking.response.AccountCreationResponse;


/**
 * @author(s) bkumar1
 * 
 * @version 1.0
 * @description: This is the controller to manage accounts
 * @since 28 Jan 2020
 */
@Api(value = "upi", description = "Accounts Controller")
@RestController
@RequestMapping(path = "/v1/accounts")
public class AccountsController {
	
	@Autowired
	private AccountCreationApi accountcreationApi;
	
	@PostMapping(path = "/", consumes = "application/json", produces = "application/json")
	@ApiOperation(value = "Creates an account", response = HashMap.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal server error") })
	public ResponseEntity<AccountCreationResponse> createAccount(@RequestBody AccountCreationRequest req) {
		return ResponseEntity.ok(accountcreationApi.callApi(req));
	}

}
