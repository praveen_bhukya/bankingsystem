/**
 * This class is a confidential property of Eltropy Technologies Pvt.Ltd
 * All rights reserved.
 */

package com.eltropy.banking.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.eltropy.banking.api.CustomerBalanceApi;
import com.eltropy.banking.api.CustomerDeleteApi;
import com.eltropy.banking.api.CustomerDetailsApi;
import com.eltropy.banking.api.CustomerKYCApi;
import com.eltropy.banking.api.CustomerRegistrationApi;
import com.eltropy.banking.request.CustomerModel;
import com.eltropy.banking.request.KYCRequest;
import com.eltropy.banking.response.CustomerBalanceResponse;
import com.eltropy.banking.response.CustomerDetailsResponse;

/**
 * @author(s) bkumar1
 * 
 * @version 1.0
 * @description: This is the controller that manages all the customer related operations
 * @since 06 July 2020
 */
@Api(value = "customer", description = "Customer Controller")
@RestController
@RequestMapping(path = "/v1/customer")
public class CustomerController {
	
	@Autowired
	private CustomerRegistrationApi customerRegistrationApi;
	@Autowired
	private CustomerKYCApi customerKYCApi;
	@Autowired
	private CustomerDetailsApi customerDetailsApi;
	@Autowired
	private CustomerDeleteApi customerDeleteApi;
	@Autowired
	private CustomerBalanceApi customerBalanceApi;
	
	@PostMapping(path = "/", consumes = "application/json", produces = "application/json")
	@ApiOperation(value = "Creates a customer", response = HashMap.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal server error") })
	public ResponseEntity<CustomerModel> createCustomer(@RequestBody CustomerModel req) {
		return ResponseEntity.ok(customerRegistrationApi.callApi(req));
	}
	
	@PostMapping(path = "/kyc", consumes = "application/json", produces = "application/json")
	@ApiOperation(value = "Updates KYC of a customer", response = HashMap.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal server error") })
	public ResponseEntity<String> updateKycOfCustomer(@RequestBody KYCRequest req) {
		return ResponseEntity.ok(customerKYCApi.callApi(req));
	}
	
	@GetMapping(path = "/", consumes = "application/json", produces = "application/json")
	@ApiOperation(value = "Gets customer details", response = HashMap.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal server error") })
	public ResponseEntity<CustomerDetailsResponse> getCustomerDetails(
			@RequestParam(value = "txnId", required = false) String req) {
		return ResponseEntity.ok(customerDetailsApi.callApi(req));
	}
	
	@DeleteMapping(path = "/", consumes = "application/json", produces = "application/json")
	@ApiOperation(value = "Deletes a customer", response = HashMap.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal server error") })
	public ResponseEntity<String> deleteCustomer(@RequestBody String customerId) {
		return ResponseEntity.ok(customerDeleteApi.callApi(customerId));
	}
	
	@GetMapping(path = "/balance", consumes = "application/json", produces = "application/json")
	@ApiOperation(value = "Gets customer account balance", response = HashMap.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal server error") })
	public ResponseEntity<CustomerBalanceResponse> getCustomerBalance(
			@RequestParam(value = "customerId", required = true) String customerId) {
		return ResponseEntity.ok(customerBalanceApi.callApi(customerId));
	}
	
	
}
